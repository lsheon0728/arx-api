#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#define MAX_FILENAME_LENGTH 256

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <folder_path>\n", argv[0]);
        return 1;
    }

    const char* folder_path = argv[1];
    DIR* dir = opendir(folder_path);

    if (dir == NULL) {
        perror("Error opening folder");
        return 1;
    }

    const char* output_header_file = "output_arrays_int8.h";
    FILE* header_file = fopen(output_header_file, "w");

    if (header_file == NULL) {
        perror("Error opening header file for writing");
        closedir(dir);
        return 1;
    }

    fprintf(header_file, "#ifndef OUTPUT_ARRAYS_H\n");
    fprintf(header_file, "#define OUTPUT_ARRAYS_H\n\n");

    struct dirent* entry;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // Check if it's a regular file
            const char* file_name = entry->d_name;
            const char* bin_extension = ".bin";
            size_t name_length = strlen(file_name);
            size_t bin_extension_length = strlen(bin_extension);

            if (name_length > bin_extension_length &&
                strcmp(file_name + name_length - bin_extension_length, bin_extension) == 0) {
                char full_file_path[MAX_FILENAME_LENGTH];
                snprintf(full_file_path, MAX_FILENAME_LENGTH, "%s/%s", folder_path, file_name);

                FILE* file = fopen(full_file_path, "rb");

                if (file == NULL) {
                    perror("Error opening file");
                    continue;
                }

                fseek(file, 0, SEEK_END);
                long file_size = ftell(file);
                fseek(file, 0, SEEK_SET);

                size_t num_elements = file_size;
                
                uint8_t* uint8_array = (uint8_t*)malloc(num_elements * sizeof(uint8_t));

                if (uint8_array == NULL) {
                    perror("Memory allocation failed");
                    fclose(file);
                    continue;
                }

                fread(uint8_array, sizeof(uint8_t), num_elements, file);
                fclose(file);

                char var_name[MAX_FILENAME_LENGTH];
                strncpy(var_name, file_name, name_length - bin_extension_length);
                var_name[name_length - bin_extension_length] = '\0';

                fprintf(header_file, "uint8_t %s[] = {", var_name);

                for (size_t i = 0; i < num_elements; i++) {
                    fprintf(header_file, "%u", uint8_array[i]);
                    if (i < num_elements - 1) {
                        fprintf(header_file, ", ");
                    }
                }

                fprintf(header_file, "};\n");
                fprintf(header_file, "size_t %s_size = %zu;\n\n", var_name, num_elements);

                free(uint8_array);
            }
        }
    }

    fprintf(header_file, "#endif // OUTPUT_ARRAYS_H\n");
    fclose(header_file);
    closedir(dir);

    printf("Binary data from %s converted to C header file and saved in %s\n", folder_path, output_header_file);

    return 0;
}
