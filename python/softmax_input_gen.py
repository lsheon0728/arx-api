import numpy as np
import sys
import torch

def create_random_8bit_int_array(length):
    # 주어진 길이만큼의 8-bit 정수 배열을 random한 값으로 생성
    return np.random.randint(0, 255, size=length, dtype=np.uint8)

def load_8bit_int_array_from_file(filename):
    # 파일에서 8-bit 정수 배열을 불러옴
    return np.fromfile(filename, dtype=np.uint8)

# def save_array_to_file(filename, data):
#     # 파일에 8-bit 정수 배열을 저장
#     data.tofile(filename)
def save_array_to_file(filename, data, is_float=False):
    if is_float:
        # Float 데이터를 float 형식의 이진 파일로 저장
        data.astype(np.float32).tofile(filename)
    else:
        # uint8 데이터를 이진 파일로 저장
        data.tofile(filename)



def softmax_arrays(array1):
    tensor = torch.from_numpy(array1.astype(np.float32))  # Numpy 배열을 PyTorch 텐서로 변환
    result = torch.softmax(tensor, dim=1)  # dim=1은 행별로 softmax를 적용함을 의미
    return result.numpy() 

def main():
    if len(sys.argv) != 3:
        length = 100
        row = 10
    else:
        length = int(sys.argv[1])
        row = int(sys.argv[2])

    # 주어진 길이만큼의 random한 8-bit integer array 생성
    array1 = create_random_8bit_int_array(length)

    # 배열을 input1.bin을 파일에 uint8로 저장
    save_array_to_file("input1.bin", array1)

    # 저장된 배열을 다시 불러옴
    array1 = load_8bit_int_array_from_file("input1.bin")

    # 1차원 배열을 2차원 배열로 변환
    array1 = array1.reshape((row, -1))

    # 배열 소프트맥스 연산
    result = softmax_arrays(array1)

    # 결과를 소수점 4자리 이하에서 버림
    #result[result < 1e-8] = 0.0
    result = np.floor(result * 1e4) / 1e4

    # 결과를 출력 파일에 float32로 저장
    #save_array_to_file("golden.bin", result)
    save_array_to_file("golden.bin", result, is_float=True)


if __name__ == "__main__":
    main()
