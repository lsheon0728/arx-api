add_executable(arx_maxpool_test
        maxpool_test.c)

add_custom_command(
        TARGET arx_maxpool_test POST_BUILD
        COMMAND python3 ${CMAKE_HOME_DIRECTORY}/python/maxpool_input_gen.py
)

target_link_libraries(arx_maxpool_test PUBLIC arx_api)

add_arxapi_test(NAME arx_maxpool_test COMMAND ${CMAKE_CURRENT_BINARY_DIR}/arx_maxpool_test USE_SH 1 PARAMS "${CMAKE_CURRENT_BINARY_DIR}/input1.bin"
        USE_DIFF 1 DIFF_TARGET ${CMAKE_CURRENT_BINARY_DIR}/golden.bin)
