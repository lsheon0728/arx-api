#ifdef RUN_ON_HOST_COMP

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../../../include/API.h"
#include "../build/include/image.h"
#include "../build/include/label.h"
#include "../build/include/param.h"

#else

#include <stdint.h>
#include "platform_info.h"
#include "ervp_printf.h"
#include "ervp_memory_util.h"
#include "ervp_multicore_synch.h"
#include "../include/API.h"
#include "../include/image.h"
#include "../include/label.h"
#include "../include/param.h"

#endif

/*
image.h, label.h, param.h
-> c_model_riscv 안에서 상대 경로(../build/header_data)와
실제 실행되는 arty-100t.release 안에서의 상대 경로(../include)가 다름.
*/

#define IMAGES_NUM 3		// The number of images to run

int main()
{
	uint8_t* w_quantize1_uint8_nhwc = (uint8_t*) malloc(8 * 1 * 5 * 5 * sizeof(uint8_t));
 	transpose_i8(w_quantize1_uint8, w_quantize1_uint8_nhwc,
				 8, 1, 5, 5,
				 8, 5, 5, 1,
				 0, 2, 3, 1);

	uint8_t* w_quantize2_uint8_nhwc = (uint8_t*) malloc(16 * 8 * 5 * 5 * sizeof(uint8_t));
	transpose_i8(w_quantize2_uint8, w_quantize2_uint8_nhwc,
				 16, 8, 5, 5,
				 16, 5, 5, 8,
				 0, 2, 3, 1);

	uint8_t* output1 = (uint8_t*) malloc(1 * 1 * 28 * 28 * sizeof(uint8_t));
	uint8_t* output2 = (uint8_t*) malloc(1 * 28 * 28 * 8 * sizeof(uint8_t));
	uint8_t* output3 = (uint8_t*) malloc(1 * 14 * 14 * 8 * sizeof(uint8_t));
	uint8_t* output4 = (uint8_t*) malloc(1 * 14 * 14 * 16 * sizeof(uint8_t));
	uint8_t* output5 = (uint8_t*) malloc(1 * 4 * 4 * 16 * sizeof(uint8_t));
	uint8_t* output6 = (uint8_t*) malloc(1 * 4 * 4 * 16 * sizeof(uint8_t));
	uint8_t* output7 = (uint8_t*) malloc(1 * 10 * sizeof(uint8_t));
	float* output8 = (float*) malloc(1 * 10 * sizeof(float));

	int image_count = 0;
	int right_answer_count = 0;
	for(int i = 0; i < IMAGES_NUM; i++) {
		quantize(image[i], output1, 784, scale1_float32, zero_point1_uint8);

		convolution_i8_scale(output1,					scale1_float32, 			zero_point1_uint8, 
							 w_quantize1_uint8_nhwc, 	w_scale1_float32, 			w_zero_point1_uint8,
							 conv_bias1_uint8, 			conv_bias1_scale_float32,	conv_bias1_zero_point_uint8,
							 output2,					scale2_float32,				zero_point2_uint8,
							 1, 28, 28, 1, 8, 5, 5,
							 2, 1, 0, 1, 28, 28);

		maxpool_i8(output2, output3, 1, 28, 28, 8, 2, 2, 0, 2);
		
		convolution_i8_scale(output3,					scale2_float32, 			zero_point2_uint8, 
							 w_quantize2_uint8_nhwc, 	w_scale2_float32, 			w_zero_point2_uint8,
							 conv_bias2_uint8, 			conv_bias2_scale_float32,	conv_bias2_zero_point_uint8,
							 output4,					scale3_float32,				zero_point3_uint8,
							 1, 14, 14, 8, 16, 5, 5,
							 2, 1, 0, 1, 14, 14);

		maxpool_i8(output4, output5, 1, 14, 14, 16, 3, 3, 0, 3);

		transpose_i8(output5, output6,
					 1, 4, 4, 16,
					 1, 16, 4, 4,
					 0, 3, 1, 2);
		
		fullyconnected_i8_scale(output6,				scale3_float32,				zero_point3_uint8,
								w_quantize3_uint8,		w_scale3_float32,			w_zero_point3_uint8,
								w_quantize4_uint8, 		w_scale4_float32,			w_zero_point4_uint8,
								output7,				scale5_float32,				zero_point5_uint8,
								1, 256, 256, 10, 1, 1, 10);

		dequantize(output7, output8, 10, scale5_float32, zero_point5_uint8, 1, 0);

		int answer = 0;
		float max_val = output8[0];

		for(int j = 0; j < 10; j++) {
			if(output8[j] > max_val) {
				max_val = output8[j];
				answer = j;
			}
		}

		if(answer == label[i]) {
			image_count++;
			right_answer_count++;
		} else {
			image_count++;
		}

		// printf("(image %d) label: %d | answer: %d | accuracy: %.4f\n", image_count, label[i], answer, (float) right_answer_count / image_count);
		printf("(image %d) label: %d | answer: %d\n", image_count, label[i], answer);
	}

	free(w_quantize1_uint8_nhwc);
	free(w_quantize2_uint8_nhwc);
	free(output1);
	free(output2);
	free(output3);
	free(output4);
	free(output5);
	free(output6);
	free(output7);
	free(output8);

	return 0;
}
