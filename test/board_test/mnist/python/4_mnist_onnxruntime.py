import os
import numpy as np
import onnxruntime as ort

# 파일에서 32-bit 실수 배열을 불러옴
def load_32bit_float_array_from_file(filename):
    return np.fromfile(filename, dtype=np.float32)

# 파일에서 8-bit 정수 배열을 불러옴
def load_8bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.int8)

# 파일에서 8-bit 부호 없는 정수 배열을 불러옴
def load_8bit_uint_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

def save_array_to_file(data, filename):
    data.tofile(filename)

# 경로 설정
image_path = './build/mnist_data/images'
label_path = './build/mnist_data/labels'
mnist_test = []

# 이미지와 라벨을 불러와 리스트에 추가
for i in range(10000):
    image_file = os.path.join(image_path, f'image{i}.bin')
    label_file = os.path.join(label_path, f'label{i}.bin')

    # 이미지 데이터 불러오기 및 형태 변환
    image_data = load_32bit_float_array_from_file(image_file).reshape(1, 1, 28, 28)

    # 라벨 데이터 불러오기
    label_data = load_8bit_int_array_from_file(label_file)

    # 이미지 데이터와 라벨 데이터를 튜플 형태로 리스트에 추가
    mnist_test.append((image_data, label_data))

# ONNX 모델 로드
session = ort.InferenceSession("./build/model/mnist-12-int8.onnx")

try:

    with open('/home/arx/rvx_release/platform/tip_arty/workspace/onnx_output.txt', 'w', encoding='utf-8') as file:

        for i in range(3):

            # 테스트 데이터셋
            input_image, output_label = mnist_test[i]
            # 모델 입출력 이름
            input_name = session.get_inputs()[0].name
            output_name = session.get_outputs()[0].name
            # 모델 추론
            golden = session.run([output_name], {input_name: input_image})

            if (output_label == np.argmax(golden[0])):
                file.write(f'(image {i + 1}) label: {np.argmax(golden[0])} | answer: {output_label[0]}\n')
        print("파일이 생성되었습니다.")

except:
    print("파일이 생성되지 않았습니다.")
