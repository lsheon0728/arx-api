import os
import onnx
from onnx import numpy_helper
import numpy as np

# 파일에서 32-bit 실수 배열을 불러옴
def load_32bit_float_array_from_file(filename):
    return np.fromfile(filename, dtype=np.float32)

# 파일에서 32-bit 정수 배열을 불러옴
def load_32bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.int32)

# 파일에서 8-bit 부호없는 정수 배열을 불러옴
def load_8bit_uint_array_from_file(filename):
    return np.fromfile(filename, dtype=np.uint8)

# 파일에서 8-bit 정수 배열을 불러옴
def load_8bit_int_array_from_file(filename):
    return np.fromfile(filename, dtype=np.int8)

# 파일에 배열을 저장
def save_array_to_file(data, filename):
    data.tofile(filename)

def dequantize(input, scale, zero_point):
    return ((input.astype(np.int32) - np.int32(zero_point)) * scale).astype(np.float32)

def quantize_int8(input, scale, zero_point):
    return np.clip(np.round(input / scale + zero_point), -128, 127).astype(np.int8)

def quantize(input, scale, zero_point):
    return np.clip(np.round(input / scale + zero_point), 0, 255).astype(np.uint8)

# 양자화 파라미터 계산
def compute_quant_params(weight, axis=None):
    if axis is not None:
        # 축을 기준으로 최소/최대 값을 구합니다.
        min_val = weight.min(axis=axis, keepdims=True)
        max_val = weight.max(axis=axis, keepdims=True)
    else:
        # 전체 배열에 대한 최소/최대 값을 구합니다.
        min_val = weight.min()
        max_val = weight.max()

    # Scale과 zero_point를 계산합니다.
    scale = (max_val - min_val) / 255
    zero_point = np.round(-min_val / scale)

    # zero_point가 uint8 범위 내에 있는지 확인하고 조정합니다.
    zero_point = np.clip(zero_point, 0, 255)

    return scale.astype(np.float32), zero_point.astype(np.uint8)


# ONNX 모델 로드
model_path = './build/model/mnist-12-int8.onnx'  # 모델 파일 경로
model = onnx.load(model_path)

# param 폴더 생성
param_dir = './build/param'
if not os.path.exists(param_dir):
    os.makedirs(param_dir)

# 추출할 가중치 이름 리스트
weights_to_extract = [
    'Input3_scale', 'Input3_zero_point',
    'Parameter5_quantized', 'Parameter5_scale', 'Parameter5_zero_point', 'Convolution28_scale', 'Convolution28_zero_point', 'ConvAddFusion_Add_B_Parameter6_quantized',
    'Parameter87_quantized', 'Parameter87_scale', 'Parameter87_zero_point', 'Convolution110_scale', 'Convolution110_zero_point', 'ConvAddFusion_Add_B_Parameter88_quantized',
    'Parameter193_reshape1_quantized', 'Parameter193_reshape1_scale', 'Parameter193_reshape1_zero_point', 'Plus214_Output_0_MatMul_scale', 'Plus214_Output_0_MatMul_zero_point',
    'Parameter194_quantized', 'Parameter194_scale', 'Parameter194_zero_point', 'Plus214_Output_0_scale', 'Plus214_Output_0_zero_point'
]

output_names = [
    'scale1_float32', 'zero_point1_uint8',
    'w_quantize1_int8', 'w_scale1_float32', 'w_zero_point1_int8', 'scale2_float32', 'zero_point2_uint8', 'conv_bias1_int32',
    'w_quantize2_int8', 'w_scale2_float32', 'w_zero_point2_int8', 'scale3_float32', 'zero_point3_uint8', 'conv_bias2_int32',
    'w_quantize3_int8', 'w_scale3_float32', 'w_zero_point3_int8', 'scale4_float32', 'zero_point4_uint8',
    'w_quantize4_uint8', 'w_scale4_float32', 'w_zero_point4_uint8', 'scale5_float32', 'zero_point5_uint8'
]

# 추출할 가중치에 대해 순회하며 저장
for idx, tensor_name in enumerate(weights_to_extract):
    for tensor in model.graph.initializer:
        if tensor.name == tensor_name:
            # NumPy 배열로 변환
            parameter_data = numpy_helper.to_array(tensor)
            # 바이너리 파일로 저장
            output_file_path = os.path.join(param_dir, output_names[idx] + '.bin')
            parameter_data.tofile(output_file_path)

# bin 파일에서 데이터 불러오기
bin_path11 = os.path.join(param_dir, output_names[0] + '.bin') # './param/Input3_scale.bin'
scale1_float32 = load_32bit_float_array_from_file(bin_path11).reshape(1)
bin_path12 =  os.path.join(param_dir, output_names[1] + '.bin') # './param/Input3_zero_point.bin'
zero_point1_uint8 = load_8bit_uint_array_from_file(bin_path12).reshape(1)

#------------------------------------------------------------------------------------------------

bin_path21 = os.path.join(param_dir, output_names[2] + '.bin') # './param/Parameter5_quantized.bin'
w_quantize1_int8 = load_8bit_int_array_from_file(bin_path21).reshape(8, 1, 5, 5)
bin_path22 = os.path.join(param_dir, output_names[3] + '.bin') # './param/Parameter5_scale.bin'
w_scale1_float32 = load_32bit_float_array_from_file(bin_path22).reshape(8)
bin_path23 = os.path.join(param_dir, output_names[4] + '.bin') # './param/Parameter5_zero_point.bin'
w_zero_point1_int8 = load_8bit_int_array_from_file(bin_path23).reshape(8)
bin_path24 = os.path.join(param_dir, output_names[5] + '.bin') # './param/Convolution28_scale.bin'
scale2_float32 = load_32bit_float_array_from_file(bin_path24).reshape(1)
bin_path25 = os.path.join(param_dir, output_names[6] + '.bin') # './param/Convolution28_zero_point.bin'
zero_point2_uint8 = load_8bit_uint_array_from_file(bin_path25).reshape(1)
bin_path26 = os.path.join(param_dir, output_names[7] + '.bin') # './param/ConvAddFusion_Add_B_Parameter6_quantized.bin'
conv_bias1_int32 = load_32bit_int_array_from_file(bin_path26).reshape(8)

w_quantize1_uint8 = (w_quantize1_int8.astype(np.int32) + 127).astype(np.uint8)
w_zero_point1_uint8 = (w_zero_point1_int8.astype(np.int32) + 127).astype(np.uint8)
conv_bias1_uint8 = (conv_bias1_int32.astype(np.int32) + 127).astype(np.uint8) # warning
conv_bias1_scale_float32 = scale1_float32 * w_scale1_float32
conv_bias1_zero_point_uint8 = np.zeros(conv_bias1_scale_float32.shape, dtype = np.uint8)

save_array_to_file(w_quantize1_uint8, os.path.join(param_dir, 'w_quantize1_uint8' + '.bin'))
save_array_to_file(w_zero_point1_uint8, os.path.join(param_dir, 'w_zero_point1_uint8' + '.bin'))
save_array_to_file(conv_bias1_uint8, os.path.join(param_dir, 'conv_bias1_uint8' + '.bin'))
save_array_to_file(conv_bias1_scale_float32, os.path.join(param_dir, 'conv_bias1_scale_float32' + '.bin'))
save_array_to_file(conv_bias1_zero_point_uint8, os.path.join(param_dir, 'conv_bias1_zero_point_uint8' + '.bin'))

#------------------------------------------------------------------------------------------------

bin_path31 = os.path.join(param_dir, output_names[8] + '.bin') # './param/Parameter87_quantized.bin'
w_quantize2_int8 = load_8bit_int_array_from_file(bin_path31).reshape(16, 8, 5, 5)
bin_path32 = os.path.join(param_dir, output_names[9] + '.bin') # './param/Parameter87_scale.bin'
w_scale2_float32 = load_32bit_float_array_from_file(bin_path32).reshape(16)
bin_path33 = os.path.join(param_dir, output_names[10] + '.bin') # './param/Parameter87_zero_point.bin'
w_zero_point2_int8 = load_8bit_int_array_from_file(bin_path33).reshape(16)
bin_path34 = os.path.join(param_dir, output_names[11] + '.bin') # './param/Convolution110_scale.bin'
scale3_float32 = load_32bit_float_array_from_file(bin_path34).reshape(1)
bin_path35 = os.path.join(param_dir, output_names[12] + '.bin') # './param/Convolution110_zero_point.bin'
zero_point3_uint8 = load_8bit_uint_array_from_file(bin_path35).reshape(1)
bin_path36 = os.path.join(param_dir, output_names[13] + '.bin') # './param/ConvAddFusion_Add_B_Parameter88_quantized.bin'
conv_bias2_int32 = load_32bit_int_array_from_file(bin_path36).reshape(16)

w_quantize2_uint8 = (w_quantize2_int8.astype(np.int32) + 127).astype(np.uint8)
w_zero_point2_uint8 = (w_zero_point2_int8.astype(np.int32) + 127).astype(np.uint8)
conv_bias2_uint8 = (conv_bias2_int32.astype(np.int32) + 127).astype(np.uint8) # warning
conv_bias2_scale_float32 = scale2_float32 * w_scale2_float32
conv_bias2_zero_point_uint8 = np.zeros(conv_bias2_scale_float32.shape, dtype = np.uint8)

save_array_to_file(w_quantize2_uint8, os.path.join(param_dir, 'w_quantize2_uint8' + '.bin'))
save_array_to_file(w_zero_point2_uint8, os.path.join(param_dir, 'w_zero_point2_uint8' + '.bin'))
save_array_to_file(conv_bias2_uint8, os.path.join(param_dir, 'conv_bias2_uint8' + '.bin'))
save_array_to_file(conv_bias2_scale_float32, os.path.join(param_dir, 'conv_bias2_scale_float32' + '.bin'))
save_array_to_file(conv_bias2_zero_point_uint8, os.path.join(param_dir, 'conv_bias2_zero_point_uint8' + '.bin'))
#-----------------------------------------------------------------------------------------------

bin_path41 = os.path.join(param_dir, output_names[14] + '.bin') # './param/Parameter193_reshape1_quantized.bin'
w_quantize3_int8 = load_8bit_int_array_from_file(bin_path41).reshape(256, 10)
bin_path42 = os.path.join(param_dir, output_names[15] + '.bin') # './param/Parameter193_reshape1_scale.bin'
w_scale3_float32 = load_32bit_float_array_from_file(bin_path42).reshape(10)
bin_path43 = os.path.join(param_dir, output_names[16] + '.bin') # './param/Parameter193_reshape1_zero_point.bin'
w_zero_point3_int8 = load_8bit_int_array_from_file(bin_path43).reshape(10)
bin_path44 = os.path.join(param_dir, output_names[17] + '.bin') # './param/Plus214_Output_0_MatMul_scale.bin'
scale4_float32 = load_32bit_float_array_from_file(bin_path44).reshape(1)
bin_path45 = os.path.join(param_dir, output_names[18] + '.bin') # './param/Plus214_Output_0_MatMul_zero_point.bin'
zero_point4_uint8 = load_8bit_uint_array_from_file(bin_path45).reshape(1)

w_quantize3_uint8 = (w_quantize3_int8.astype(np.int32) + 127).astype(np.uint8)
w_zero_point3_uint8 = (w_zero_point3_int8.astype(np.int32) + 127).astype(np.uint8)

save_array_to_file(w_quantize3_uint8, os.path.join(param_dir, 'w_quantize3_uint8' + '.bin'))
save_array_to_file(w_zero_point3_uint8, os.path.join(param_dir, 'w_zero_point3_uint8' + '.bin'))

#-----------------------------------------------------------------------------------------------

bin_path51 = os.path.join(param_dir, output_names[19] + '.bin') # './param/Parameter194_quantized.bin'
w_quantize4_uint8 = load_8bit_uint_array_from_file(bin_path51).reshape(1, 10)
bin_path52 = os.path.join(param_dir, output_names[20] + '.bin') # './param/Parameter194_scale.bin'
w_scale4_float32 = load_32bit_float_array_from_file(bin_path52).reshape(1)
bin_path53 = os.path.join(param_dir, output_names[21] + '.bin') # './param/Parameter194_zero_point.bin'
w_zero_point4_uint8 = load_8bit_uint_array_from_file(bin_path53).reshape(1)
bin_path54 = os.path.join(param_dir, output_names[22] + '.bin') # './param/Plus214_Output_0_scale.bin'
scale5_float32 = load_32bit_float_array_from_file(bin_path54).reshape(1)
bin_path55 = os.path.join(param_dir, output_names[23] + '.bin') # './param/Plus214_Output_0_zero_point.bin'
zero_point5_uint8 = load_8bit_uint_array_from_file(bin_path55).reshape(1)


