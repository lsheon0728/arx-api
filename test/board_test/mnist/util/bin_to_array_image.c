#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>

#define MAX_FILENAME_LENGTH 256
#define NUM_IMAGE 10
#define SIZE_IMAGE 784

const char* HEADER_PATH = "build/include";
const char* SOURCE_PATH = "build/lib";

int write_header_file(const char* file_name);
int write_source_file(const char* folder_path, const char* file_name);
int open_header_file(FILE** header_file, const char* file_name);
int open_source_file(FILE** source_file, const char* file_name);
void write_preprocessors_beginning(FILE* header_file, const char* file_name);
void write_preprocessors_ending(FILE* header_file, const char* file_name);
void get_header_file_name_capital(const char* file_name, char** header_file_name_capital);
int write_source_file_lines(const char* folder_path, FILE* source_file);
int get_file_num(const char* file_name);

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <folder_path> <file_name>\n", argv[0]);
        return 1;
    }

    const char* folder_path = argv[1];
    const char* file_name = argv[2];
 
    // Write header file
    if(write_header_file(file_name) == 1) {
        perror("Error opening file");
        return 1;
    }

    // Write source file
    if(write_source_file(folder_path, file_name) == 1) {
        perror("Error opening file");
        return 1;
    }

    printf("Binary data from %s converted to C header file\n", folder_path);

    return 0;
}

int write_header_file(const char* file_name) {
    FILE* header_file;

    if(open_header_file(&header_file, file_name) == 1) {
        return 1;
    };
    
    write_preprocessors_beginning(header_file, file_name);
    
    fprintf(header_file, "extern float image[%d][%d];\n\n", NUM_IMAGE, SIZE_IMAGE);

    write_preprocessors_ending(header_file, file_name);

    fclose(header_file);
    return 0;   
}

int open_header_file(FILE** header_file, const char* file_name) {
    const char* header_extension = ".h";
    const char* slash = "/";

    size_t file_path_length = strlen(HEADER_PATH) + strlen(slash) + strlen(file_name) + strlen(header_extension) + 1;
    char* file_path = (char*) malloc(file_path_length * sizeof(char));

    strcpy(file_path, HEADER_PATH);
    strcat(file_path, slash);
    strcat(file_path, file_name);
    strcat(file_path, header_extension);

    *header_file = fopen(file_path, "w");

    free(file_path);

    if (*header_file == NULL) {
        perror("Error opening header file for writing");
        return 1;
    }

    return 0;
}

int open_source_file(FILE** source_file, const char* file_name) {
    const char* source_extension = ".c";
    const char* slash = "/";

    size_t file_path_length = strlen(SOURCE_PATH) + strlen(slash) + strlen(file_name) + strlen(source_extension) + 1;
    char* file_path = (char*) malloc(file_path_length * sizeof(char));

    strcpy(file_path, SOURCE_PATH);
    strcat(file_path, slash);
    strcat(file_path, file_name);
    strcat(file_path, source_extension);

    *source_file = fopen(file_path, "w");

    free(file_path);

    if (*source_file == NULL) {
        return 1;
    }

    return 0;
}

void write_preprocessors_beginning(FILE* header_file, const char* file_name) {
    char* header_file_name_capital;

    get_header_file_name_capital(file_name, &header_file_name_capital);

    fprintf(header_file, "#ifndef %s\n", header_file_name_capital);
    fprintf(header_file, "#define %s\n\n", header_file_name_capital);

    free(header_file_name_capital);
}

void write_preprocessors_ending(FILE* header_file, const char* file_name) {
    char* header_file_name_capital;
    get_header_file_name_capital(file_name, &header_file_name_capital);

    fprintf(header_file, "#endif // %s", header_file_name_capital);

    free(header_file_name_capital);
}

void get_header_file_name_capital(const char* file_name, char** header_file_name_capital) {
    const char* header_extension = ".h";

    size_t length = strlen(file_name) + strlen(header_extension) + 1;
    char* header_file_name = (char*) malloc(length * sizeof(char));

    strcpy(header_file_name, file_name);
    strcat(header_file_name, header_extension);

    *header_file_name_capital = (char*) malloc(length * sizeof(char));
    strcpy(*header_file_name_capital, header_file_name);

    for(int i = 0; (*header_file_name_capital)[i] != '\0'; i++) {
        if((*header_file_name_capital)[i] == '_') {
            continue;
        }
        else if((*header_file_name_capital)[i] == '.') {
            (*header_file_name_capital)[i] = '_';
        } else {
            (*header_file_name_capital)[i] -= 32;
        }
    }

    free(header_file_name);
}

int write_source_file(const char* folder_path, const char* file_name) {
    FILE* source_file;
    if(open_source_file(&source_file, file_name) == 1) {
        return 1;
    }

    fprintf(source_file, "#include \"../include/%s.h\"\n\n", file_name);

    if(write_source_file_lines(folder_path, source_file)) {
        return 1;
    }

    fclose(source_file);

    return 0;   
}

int write_source_file_lines(const char* folder_path, FILE* source_file) {
    DIR* dir = opendir(folder_path);

    if (dir == NULL) {
        perror("Error opening folder");
        return 1;
    }
    struct dirent* entry;

    float** data = (float**) malloc(NUM_IMAGE * sizeof(float*));
    for(int i = 0; i < NUM_IMAGE; i++) {
        data[i] = (float*) malloc(SIZE_IMAGE * sizeof(float));
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // Check if it's a regular file
            const char* bin_file_name = entry->d_name;
            const char* bin_extension = ".bin";
            size_t name_length = strlen(bin_file_name);
            size_t bin_extension_length = strlen(bin_extension);

            int file_num = get_file_num(bin_file_name);

            if (name_length > bin_extension_length &&
                strcmp(bin_file_name + name_length - bin_extension_length, bin_extension) == 0) {
                char full_file_path[MAX_FILENAME_LENGTH];
                snprintf(full_file_path, MAX_FILENAME_LENGTH, "%s/%s", folder_path, bin_file_name);

                FILE* file = fopen(full_file_path, "rb");

                if (file == NULL) {
                    perror("Error opening file");
                    continue;
                }

                fseek(file, 0, SEEK_END);
                long file_size = ftell(file);
                fseek(file, 0, SEEK_SET);

                size_t num_elements = file_size / sizeof(float);
                
                float* float_array = (float*)malloc(num_elements * sizeof(float));

                if (float_array == NULL) {
                    perror("Memory allocation failed");
                    fclose(file);
                    continue;
                }

                fread(float_array, sizeof(float), num_elements, file);
                fclose(file);

                char var_name[MAX_FILENAME_LENGTH];
                strncpy(var_name, bin_file_name, name_length - bin_extension_length);
                var_name[name_length - bin_extension_length] = '\0';


                for (size_t i = 0; i < num_elements; i++) {
                    if(file_num < NUM_IMAGE) {
                        data[file_num][i] = float_array[i];
                    }
                }

                free(float_array);
            }
        }
    }


    fprintf(source_file, "float image[%d][%d] = {\n", NUM_IMAGE, SIZE_IMAGE);
    
    for(int i = 0; i < NUM_IMAGE; i++) {
        fprintf(source_file, "{");
        for(int j = 0; j < SIZE_IMAGE; j++) {
            if(j != SIZE_IMAGE - 1) {
                fprintf(source_file, "%0.1f, ", data[i][j]);
            } else {
                fprintf(source_file, "%0.1f", data[i][j]);
            }
        }
        if(i != NUM_IMAGE) {
            fprintf(source_file, "},\n");
        } else {
            fprintf(source_file, "}\n");
        } 
    }
    fprintf(source_file, "};");

    for(int i = 0; i < NUM_IMAGE; i++) {
        free(data[i]);
    }
    free(data);

    closedir(dir);
}

int get_file_num(const char* file_name) {
    int length = strlen(file_name);
    char* output_str = (char*) malloc(length * sizeof(char));

    int ind = 0;
    for(int i = 0; i < length; i++) {
        if(isdigit((unsigned char)file_name[i])) {
            output_str[ind] = (unsigned char)file_name[i];
            ind++;
        }
    }
    output_str[ind] = '\0';

    int output_int = atoi(output_str);

    free(output_str);
    return output_int;   
}