#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>

#define MAX_FILENAME_LENGTH 256

const char* HEADER_PATH = "build/include";
const char* SOURCE_PATH = "build/lib";

int write_header_file(const char* file_name);
int write_source_file(const char* folder_path, const char* file_name);
int open_header_file(FILE** header_file, const char* file_name);
int open_source_file(FILE** source_file, const char* file_name);
void write_preprocessors_beginning(FILE* header_file, const char* file_name);
void write_preprocessors_ending(FILE* header_file, const char* file_name);
void get_header_file_name_capital(const char* file_name, char** header_file_name_capital);
int write_source_file_lines(const char* folder_path, FILE* source_file);
int get_file_num(const char* file_name);

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <folder_path> <file_name>\n", argv[0]);
        return 1;
    }

    const char* folder_path = argv[1];
    const char* file_name = argv[2];

    const char* header_extension = ".h";
    const char* source_extension = ".c";
    const char* slash = "/";

    size_t header_file_path_length = strlen(HEADER_PATH) + strlen(slash) + strlen(file_name) + strlen(header_extension) + 1;
    char* header_file_path = (char*) malloc(header_file_path_length * sizeof(char));

    strcpy(header_file_path, HEADER_PATH);
    strcat(header_file_path, slash);
    strcat(header_file_path, file_name);
    strcat(header_file_path, header_extension);

    size_t source_file_path_length = strlen(SOURCE_PATH) + strlen(slash) + strlen(file_name) + strlen(source_extension) + 1;
    char* source_file_path = (char*) malloc(source_file_path_length * sizeof(char));

    strcpy(source_file_path, SOURCE_PATH);
    strcat(source_file_path, slash);
    strcat(source_file_path, file_name);
    strcat(source_file_path, source_extension);

    FILE* header_file = fopen(header_file_path, "w");
    FILE* source_file = fopen(source_file_path, "w");

    free(header_file_path);
    free(source_file_path);

    DIR* dir = opendir(folder_path);

    if (dir == NULL) {
        perror("Error opening folder");
        return 1;
    }

    if (header_file == NULL) {
        perror("Error opening header file for writing");
        closedir(dir);
        return 1;
    }

    size_t header_file_name_length = strlen(file_name) + strlen(header_extension) + 1;
    char* header_file_name = (char*) malloc(header_file_name_length * sizeof(char));

    strcpy(header_file_name, file_name);
    strcat(header_file_name, header_extension);

    char* header_file_name_capital = (char*) malloc(header_file_name_length * sizeof(char));
    strcpy(header_file_name_capital, header_file_name);

    for(int i = 0; header_file_name_capital[i] != '\0'; i++) {
        if(header_file_name_capital[i] == '_') {
            continue;
        }
        else if(header_file_name_capital[i] == '.') {
            header_file_name_capital[i] = '_';
        } else {
            header_file_name_capital[i] -= 32;
        }
    }

    fprintf(header_file, "#ifndef %s\n", header_file_name_capital);
    fprintf(header_file, "#define %s\n\n", header_file_name_capital);

    fprintf(header_file, "#include <stdint.h>\n\n");
    fprintf(source_file, "#include \"../include/%s\"\n\n", header_file_name);

    struct dirent* entry;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // Check if it's a regular file
            const char* file_name = entry->d_name;
            const char* bin_extension = ".bin";
            size_t name_length = strlen(file_name);
            size_t bin_extension_length = strlen(bin_extension);

            if (name_length > bin_extension_length &&
                strcmp(file_name + name_length - bin_extension_length, bin_extension) == 0) {
                char full_file_path[MAX_FILENAME_LENGTH];
                snprintf(full_file_path, MAX_FILENAME_LENGTH, "%s/%s", folder_path, file_name);

                FILE* file = fopen(full_file_path, "rb");

                if (file == NULL) {
                    perror("Error opening file");
                    continue;
                }

                fseek(file, 0, SEEK_END);
                long file_size = ftell(file);
                fseek(file, 0, SEEK_SET);

                size_t num_elements = 0;
                void* data_array = NULL;
                const char* data_type = NULL;

                if (strstr(file_name, "uint8") != NULL) {
                    data_type = "uint8_t";
                    num_elements = file_size / sizeof(uint8_t);
                    data_array = malloc(file_size);
                    fread(data_array, sizeof(uint8_t), num_elements, file);
                } else if (strstr(file_name, "int8") != NULL) {
                    data_type = "char";
                    num_elements = file_size / sizeof(char);
                    data_array = malloc(file_size);
                    fread(data_array, sizeof(char), num_elements, file);
                } else if (strstr(file_name, "int32") != NULL) {
                    data_type = "int";
                    num_elements = file_size / sizeof(int);
                    data_array = malloc(file_size);
                    fread(data_array, sizeof(int), num_elements, file);
                } else if (strstr(file_name, "float32") != NULL) {
                    data_type = "float";
                    num_elements = file_size / sizeof(float);
                    data_array = malloc(file_size);
                    fread(data_array, sizeof(float), num_elements, file);
                }

                if (data_array == NULL || data_type == NULL) {
                    perror("Memory allocation failed or unknown data type");
                    fclose(file);
                    continue;
                }

                char var_name[MAX_FILENAME_LENGTH];
                strncpy(var_name, file_name, name_length - bin_extension_length);
                var_name[name_length - bin_extension_length] = '\0';

                fprintf(header_file, "extern %s %s[%ld];\n", data_type, var_name, num_elements);
                fprintf(source_file, "%s %s[] = {", data_type, var_name);

                for (size_t i = 0; i < num_elements; i++) {
                    if (strcmp(data_type, "char") == 0) {
                        fprintf(source_file, "%d", ((char*)data_array)[i]);
                    } else if (strcmp(data_type, "uint8_t") == 0) {
                        fprintf(source_file, "%u", ((uint8_t*)data_array)[i]);
                    } else if (strcmp(data_type, "int") == 0) {
                        fprintf(source_file, "%d", ((int*)data_array)[i]);
                    } else if (strcmp(data_type, "float") == 0) {
                        fprintf(source_file, "%f", ((float*)data_array)[i]);
                    }

                    if (i < num_elements - 1) {
                        fprintf(source_file, ", ");
                    }
                }

                fprintf(source_file, "};\n");

                free(data_array);
                fclose(file);
            }
        }
    }

    fprintf(header_file, "#endif // %s\n", header_file_name_capital);
    
    free(header_file_name);
    free(header_file_name_capital);
    fclose(source_file);
    fclose(header_file);
    closedir(dir);

    printf("Binary data from %s converted to C header file\n", folder_path);

    return 0;
}